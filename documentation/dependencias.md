* Twython (3.1.2)
* durus (3.9)
* WX Python (2.9.x para windows y 2.8.x para GNU/Linux)
* Suds (0.4)
* Pycurl
* Configobj (4.7.2)
* Py2exe (solo Windows)
* GStreamer Python 0.10 (solo GNU/Linux)
