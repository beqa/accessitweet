from twython import Twython, TwythonError
import config

class twitter(object):
 def __init__(self):
  pass

 def login(self):
  self.twitter = Twython(config.main["twitter"]["app_key"], config.main["twitter"]["app_secret"], config.main["twitter"]["user_key"], config.main["twitter"]["user_secret"])
  self.twitter.verify_credentials()

 def get_url(self):
  twitter = Twython(config.main["twitter"]["app_key"], config.main["twitter"]["app_secret"])
  auth = twitter.get_authentication_tokens()
  self.oauth_token = auth['oauth_token']
  self.oauth_token_secret = auth['oauth_token_secret']
  return auth['auth_url']

 def authorize(self, verifier):
  self.twitter = Twython(config.main["twitter"]["app_key"], config.main["twitter"]["app_secret"], self.oauth_token, self.oauth_token_secret)
  final_step = self.twitter.get_authorized_tokens(verifier)
  config.main['twitter']['user_key'] = final_step['oauth_token']
  config.main['twitter']['user_secret'] = final_step['oauth_token_secret']
  config.main.write()

