# -*- coding: utf-8 -*-
import re
import htmlentitydefs
import datetime
import time

months = {
"January": u"January",
"February": u"February",
"March": u"March",
"April": u"April",
"May": u"May",
"June": u"June",
"July": u"July",
"August": u"August",
"September": u"September",
"October": u"October",
"November": u"november","December": u"December",
}

days = {"Sunday": u"Sunday",
"Monday": u"Monday",
"Tuesday": u"Tuesday",
"Wednesday": u"Wednesday",
"Thursday": u"Thursday",
"Friday": u"Friday",
"Saturday": u"Saturday"}

def StripChars(s):
 """Converts any html entities in s to their unicode-decoded equivalents and returns a string."""
 entity_re = re.compile(r"&(#\d+|\w+);")
 def matchFunc(match):
  """Nested function to handle a match object.
 If we match &blah; and it's not found, &blah; will be returned.
 if we match #\d+, unichr(digits) will be returned.
 Else, a unicode string will be returned."""
  if match.group(1).startswith('#'): return unichr(int(match.group(1)[1:]))
  replacement = htmlentitydefs.entitydefs.get(match.group(1), "&%s;" % match.group(1))
  return replacement.decode('iso-8859-1')
 return unicode(entity_re.sub(matchFunc, s))

def translate(string):
 global months, days
 for d in months:
  string = string.replace(d, months[d])
 for d in days:
  string = string.replace(d, days[d])
 return string

chars = "abcdefghijklmnopqrstuvwxyz"

def compose_tweet(tweet, config):
 original_date = datetime.datetime.strptime(tweet["created_at"], '%a %b %d %H:%M:%S +0000 %Y')
 date = original_date-datetime.timedelta(seconds=time.timezone)
 ts = translate(datetime.datetime.strftime(date, '%A, %d of %B,  %Y, %H:%M:%S'))
# ts = tweet["created_at"]
 text = StripChars(tweet["text"])
 if tweet.has_key("sender"):
  source = "DM"
  if config.settings["user_name"] == tweet["sender"]["screen_name"]: user = "Dm to %s" % (tweet["recipient"]["name"])
  else: user = tweet["sender"]["name"]
 elif tweet.has_key("user"):
  user = tweet["user"]["name"]
  source = re.sub(r"(?s)<.*?>", " ", tweet["source"])
  try: text = "rt @%s: %s" % (tweet["retweeted_status"]["user"]["screen_name"], StripChars(tweet["retweeted_status"]["text"]))
  except KeyError: text = "%s" % (StripChars(tweet["text"]))
  if text[-1] in chars: text=text+"."
# if text[-1] != ".": text = text + "."
 return [user+": ", text, ts+": ", source]

def compose_followers_list(tweet, config):
 original_date = datetime.datetime.strptime(tweet["created_at"], '%a %b %d %H:%M:%S +0000 %Y')
 date = original_date-datetime.timedelta(seconds=time.timezone)
 ts = translate(datetime.datetime.strftime(date, '%A, %d of %B, %Y, %H:%M:%S'))
# ts = tweet["created_at"]
 if tweet.has_key("status"):
  if len(tweet["status"]) > 4:
   original_date2 = datetime.datetime.strptime(tweet["status"]["created_at"], '%a %b %d %H:%M:%S +0000 %Y')
   date2 = original_date2-datetime.timedelta(seconds=time.timezone)
   ts2 = translate(datetime.datetime.strftime(date2, '%A, %d of %B, %Y, %H:%M:%S'))
#   ts2 = "Not Available"
 else:
  ts2 = "Not Available"
 return ["", u"%s (@%s). %s followers, %s friends, %s tweets. Last Tweeted %s. Joined Twitter %s" % (tweet["name"], tweet["screen_name"], tweet["followers_count"], tweet["friends_count"],  tweet["statuses_count"], ts2, ts)]

def compose_event(data):
 if data["event"] == "block":
  event = _("You blocked %s") % (target["name"])
 elif data["event"] == "unblock":
  event = _(u"You unblocked %s") % (target["name"])
 elif data["event"] == "list_created":
  event = _(u"You created a list.")
 elif data["event"] == "list_destroyed":
  event = _("You removed a list")
 elif data["event"] == "list_updated":
  event = _("You updated a list")
 else: event = "algo"
 return [time.strftime("%H:%M:%S"), event]