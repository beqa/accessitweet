# -*- coding: utf-8 -*-
from twitter import compose, utils
from twython import TwythonStreamer
import sound
from mysc import event
import wx
import config
import logging as original_logger
log = original_logger.getLogger("MainStream")
import output

class streamer(TwythonStreamer):
 def __init__(self, app_key, app_secret, oauth_token, oauth_token_secret, timeout=300, retry_count=None, retry_in=10, client_args=None, handlers=None, chunk_size=1, parent=None):
  self.db = parent.db
  self.parent = parent
  TwythonStreamer.__init__(self, app_key, app_secret, oauth_token, oauth_token_secret, timeout=60, retry_count=0, retry_in=60, client_args=None, handlers=None, chunk_size=1)
  log.debug("Stream inicializado.")

 def on_error(self, status_code, data):
  log.debug("Error %s: %s" % (status_code, data))

 def check_send(self, data):
  if self.db.settings["user_name"] == data["user"]["screen_name"]:
   if config.main["streams"]["reverse_timelines"] == False: self.db.settings["sent"].append(data)
   else: self.db.settings["sent"].insert(0, data)
   wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("sent")), event.ResultEvent())

 def check_favs(self, data):
  if data["source"]["screen_name"] == self.db.settings["user_name"]:
   if config.main["streams"]["reverse_timelines"] == False: self.db.settings["favs"].append(data["target_object"])
   else: self.db.settings["favs"].insert(0, data["target_object"])
   wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("favs")), event.ResultEvent())
   deleted_event = event.event(event.EVT_DELETED, 1)
   deleted_event.SetItem(["no disponible", "Alguien le has dado fav"])
   wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("events")), deleted_event)
  else:
   output.speak(_(u"%s(@%s) has marked as favorite: %s") % (data["source"]["name"], data["source"]["screen_name"], data["target_object"]["text"]))

 def check_mentions(self, data):
  if 'text' in data:
   if self.db.settings["user_name"] in data["text"]:
    if config.main["streams"]["reverse_timelines"] == False: self.db.settings["mentions"].append(data)
    else: self.db.settings["mentions"].insert(0, data)
    wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("mentions")), event.ResultEvent())
    output.speak(u"Mention from %s " % (data["user"]["name"]))
    self.parent.sound.play("mention_received.wav")

 def process_dm(self, data):
  if self.db.settings["user_name"] == data["direct_message"]["sender"]["screen_name"]:
   if config.main["streams"]["reverse_timelines"] == False: self.db.settings["sent"].append(data["direct_message"])
   else: self.db.settings["sent"].insert(0, data["direct_message"])
   wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("sent")), event.ResultEvent())
  if self.db.settings["user_name"] != data["direct_message"]["sender"]["screen_name"]:
   if config.main["streams"]["reverse_timelines"] == False: self.db.settings["direct_messages"].append(data["direct_message"])
   else: self.db.settings["direct_messages"].insert(0, data["direct_message"])
   wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("direct_messages")), event.ResultEvent())
   output.speak(u"Direct Message")
   self.parent.sound.play("dm_received.wav")

 def check_follower(self, data):
  if data["target"]["screen_name"] == self.db.settings["user_name"]:
   output.speak(u"%s has followed you." % (data["source"]["name"]))
   if config.main["buffers"]["followers"] == True:
    if config.main["streams"]["reverse_timelines"] == False: self.db.settings["followers"].append(data["source"])
    else: self.db.settings["followers"].insert(0, data["source"])
    wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("followers")), event.ResultEvent())
    self.parent.sound.play("update_followers.wav")
  elif data["source"]["screen_name"] == self.db.settings["user_name"] and config.main["buffers"]["friends"] == True:
   if config.main["streams"]["reverse_timelines"] == False: self.db.settings["friends"].append(data["target"])
   else: self.db.settings["friends"].insert(0, data["target"])
   self.friends.append(data["target"]["id"])
   wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("friends")), event.ResultEvent())

 def remove_fav(self, data):
  if self.db.settings["user_name"] == data["source"]["screen_name"]:
   self.db.settings.update()
   item = utils.find_item(data["target_object"]["id"], self.db.settings["favs"])
   self.db.settings["favs"].pop(item)
   deleted_event = event.event(event.EVT_DELETED, 1)
   deleted_event.SetItem(item)
   wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("favs")), deleted_event)
   self.parent.sound.play("favorite.wav")

 def remove_friend(self, data):
  if config.main["buffers"]["friends"] == True:
   item = utils.find_item(data["target"]["id"], self.db.settings["friends"])
   if item > 0:
    deleted_event = event.event(event.EVT_DELETED, 1)
    deleted_event.SetItem(item)
    self.friends.pop(item)
    wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("friends")), deleted_event)

 def on_success(self, data):
  if "direct_message" in data:
   self.process_dm(data)
  elif "friends" in data:
   self.friends = data["friends"]
  elif "text" in data:
   self.check_mentions(data)
   self.check_send(data)
   if data["user"]["id"] in self.friends or data["user"]["screen_name"] == self.db.settings["user_name"]:
    if config.main["streams"]["reverse_timelines"] == False: self.db.settings["home_timeline"].append(data)
    else: self.db.settings["home_timeline"].insert(0, data)
    wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("home_timeline")), event.ResultEvent())
    self.parent.sound.play("tweet_received.wav")
  elif data.has_key("event"):
   if "favorite" == data["event"] and config.main["buffers"]["favorites"] == True:
    self.check_favs(data)
   elif "unfavorite" == data["event"] and config.main["buffers"]["favorites"] == True:
    self.remove_fav(data)
   elif "follow" == data["event"] and config.main["buffers"]["followers"] == True:
    self.check_follower(data)
   elif "unfollow" == data["event"]:
    self.remove_friend(data)
   else:
    evento = compose.compose_event(data)
    deleted_event = event.event(event.EVT_DELETED, 1)
    deleted_event.SetItem(evento)
    wx.PostEvent(self.parent.nb.GetPage(self.db.settings["buffers"].index("events")), deleted_event)