from twython import Twython, TwythonError
import config
import sound

def call_paged(update_function, twitter_object, *args, **kwargs):
 max = config.main["twitter"]["max_api_calls"]-1
 results = []
 data = update_function(*args, **kwargs)
 results.extend(data)
 for i in range(0, max):
  if i == 0: max_id = results[-1]["id"]
  else: max_id = results[0]["id"]
  data = update_function(max_id=max_id, *args, **kwargs)
  results.extend(data)
 results.reverse()
 return results

def start_user_info(config, twitter):
 f = twitter.twitter.get_account_settings()
 sn = f["screen_name"]
 config.settings["user_name"] = sn

def start_stream(db, twitter, name, function, param=None, sndFile=""):
 num = 0
 sounded = False
 if db.settings.has_key(name):
  try:
   if db.settings[name][0]["id"] > db.settings[name][-1]["id"]:
    last_id = db.settings[name][0]["id"]
   else:
    last_id = db.settings[name][-1]["id"]
  except IndexError:
   pass
  if param != None:
   tl = call_paged(function, twitter, count=200, sinze_id=last_id, screen_name=param)
  else:
   tl = call_paged(function, twitter, count=200, sinze_id=last_id)
 else:
  if param != None:
   tl = call_paged(function, twitter, count=200,  screen_name=param)
  else:
   tl = call_paged(function, twitter, count=200)
  db.settings[name] = []
  last_id = 0
 if len(db.settings[name]) > 0:
  for i in tl:
   if int(i["id"]) > int(last_id):
    if config.main["streams"]["reverse_timelines"] == False: db.settings[name].append(i)
    else: db.settings[name].insert(0, i)
    sounded = True
    num = num+1
 elif len(db.settings[name]) == 0:
  for i in tl:
   if config.main["streams"]["reverse_timelines"] == False: db.settings[name].append(i)
   else: db.settings[name].insert(0, i)
   sounded = True
   num = num+1
 if sounded == True:
  sound.play(sndFile)
 db.settings.update()
 return num

def start_followers(db, twitter, name, function, param=None, sndFile=""):
 num = 0
 sounded = False
 db.settings[name] = []
 next_cursor = -1
 while(next_cursor):
  tl = function(screen_name=param, count=200, cursor=next_cursor)
  for i in tl['users']:
   db.settings[name].append(i)
   sounded = True
   num = num+1
  next_cursor = tl["next_cursor"]
 if config.main["streams"]["reverse_timelines"] == False: db.settings[name].reverse()
 if sounded == True:
  sound.play(sndFile)
 return num

def update_stream(config, twitter, name, function, param=None, sndFile=""):
 num = 0
 sounded = False
 tl = function(count=200, sinze_id=config.settings[name][-1]["id"], screen_name=param)
 tl.reverse()
 for i in tl:
  if i["id"] > config.settings[name][-1]["id"]:
   config.settings[name].append(i)
   sounded = True
   num = num+1
 if sounded == True:
  sound.play(sndFile)
 return num

def start_sent(db, twitter, name, function, param=None, sndFile=""):
 num = 0
 sounded = False
 if db.settings.has_key(name):
  try:
   if db.settings[name][0]["id"] > db.settings[name][-1]["id"]:
    last_id = db.settings[name][0]["id"]
   else:
    last_id = db.settings[name][-1]["id"]
  except IndexError:
   return 0
  if param != None:
   tl = function(count=200, sinze_id=last_id, screen_name=param)
   tl2 = twitter.twitter.get_sent_messages(count=200, sinze_id=last_id)
  else:
   tl = function(count=200, sinze_id=last_id)
   tl2 = twitter.twitter.get_sent_messages(count=200, sinze_id=last_id)
 else:
  if param != None:
   tl = function(count=200, screen_name=param)
   tl2 = twitter.twitter.get_sent_messages(count=200)
  else:
   tl = function(count=200)
   tl2 = twitter.twitter.get_sent_messages(count=200, sinze_id=last_id)
  db.settings[name] = []
  last_id = 0
 tl.extend(tl2)
# tl.reverse()
 tl.sort(key=lambda tup: tup["id"]) 
 if len(db.settings[name]) > 0:
  for i in tl:
#   print last_id, i["id"]
   if int(i["id"]) > int(last_id):
    if config.main["streams"]["reverse_timelines"] == False: db.settings[name].append(i)
    else: db.settings[name].insert(0, i)
    sounded = True
    num = num+1
 elif len(db.settings[name]) == 0:
  for i in tl:
   if config.main["streams"]["reverse_timelines"] == False: db.settings[name].append(i)
   else: db.settings[name].insert(0, i)
   sounded = True
   num = num+1
 if sounded == True:
#   print "playing "+sndFile
  sound.play(sndFile)
 return num