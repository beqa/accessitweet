# -*- coding: cp1252 -*-
""" TW Blue

A twitter accessible, easy of use and cross platform application."""
import config
import gui
import wx
import sound_lib
import paths
import output
import gettext
import platform
if platform.system() == "Windows":
 from logger import logger as logging
 import gettext_windows
 gettext_windows.setup_env()
gettext.install("twblue", paths.locale_path(), unicode=True)

# Inits the sound_lib module
sound_lib.output.Output()
config.setup()
output.setup()
app = wx.App()
#app = wx.App(redirect=True, useBestVisual=True, filename=paths.logs_path('tracebacks.log'))
frame = gui.main.mainFrame().Show()
app.MainLoop()
