# -*- coding: utf-8 -*-
import sys, url_shortener, audio_services
import config
import sound_lib
import os
import logging as original_logger 
log = original_logger.getLogger("sound")
import subprocess
import paths
import wx
from mysc.repeating_timer import RepeatingTimer

def play(filename, option=False):
 if sys.platform == "linux2":
  import os
  os.system("play sounds/%s -q" % (filename))
 elif sys.platform == "win32":
  from wx import Sound, SOUND_SYNC
  if option == False:
   Sound("sounds/"+filename).Play(SOUND_SYNC)
  else:
   Sound("sounds/"+filename).Play()

def recode_audio(filename, quality=4.5):
 subprocess.call(r'"%s" -q %r "%s"' % (paths.app_path('oggenc2.exe'), quality, filename))

def recording(filename):
 try:
  val = sound_lib.recording.WaveRecording(filename=filename)
 except sound_lib.main.BassError:
  sound_lib.input.Input()
  val = sound_lib.recording.WaveRecording(filename=filename)
 return val

class sounds(object):
 def __init__(self):
  self.files = []
  self.cleaner = RepeatingTimer(60, self.clear_list)
  self.cleaner.start()

 def clear_list(self):
  log.debug("Cleaning sounds... Total sounds found: %i" % len(self.files))
  if self.files == []: return
  for i in range(0, len(self.files)):
   if self.files[i].is_playing == False:
    self.files.pop(i)
  log.debug("Files used now: %i" % len(self.files))

 def play(self, sound, argument=False):
  sound_object = sound_lib.stream.FileStream(file=paths.sound_path(sound))
  sound_object.volume = config.main["mysc"]["volume"]
  self.files.append(sound_object)
  sound_object.play()

class urlStream(object):
 def __init__(self, url):
  self.url = url
  log.debug(u"URL: %s" % url)
  self.prepared = False

 def prepare(self):
  self.prepared = False
  url = url_shortener.unshorten(self.url)
  log.debug("url desacortada: "+str(url))
  if url != None:
   self.url = url
  transformer = audio_services.find_url_transformer(self.url)
  self.url = transformer(self.url)
  log.debug(u"Url transformada: %s" % self.url)
  prepare = True

 def play(self):
  self.stream = sound_lib.stream.URLStream(url=self.url)
  self.stream.volume = config.main["mysc"]["volume"]
  self.stream.play()

 @staticmethod
 def delete_old_tempfiles():
  for f in glob(os.path.join(tempfile.gettempdir(), 'tmp*.wav')):
   try:
    os.remove(f)
   except:
    pass

