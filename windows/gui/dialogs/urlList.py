# -*- coding: utf-8 -*-
import wx
import webbrowser
import url_shortener

class urlList(wx.Dialog):
 def __init__(self, urls):
  self.urls = urls
  wx.Dialog.__init__(self, None)
  panel = wx.Panel(self)
  label = wx.StaticText(panel, -1, _(u"Select a URL"))
  self.lista = wx.ListBox(panel, -1)
  self.lista.SetFocus()
  sizer = wx.BoxSizer(wx.VERTICAL)
  sizer.Add(label)
  sizer.Add(self.lista)
  goBtn = wx.Button(panel, wx.ID_OK, _("OK"))
  goBtn.Bind(wx.EVT_BUTTON, self.onGo)
  cancelBtn = wx.Button(panel, wx.ID_CANCEL, _(u"Cancel"))
  btnSizer = wx.BoxSizer()
  btnSizer.Add(goBtn)
  btnSizer.Add(cancelBtn)
  panel.SetSizer(sizer)
  self.populate_list()
  self.lista.SetSelection(0)

 def onGo(self, ev):
  webbrowser.open_new_tab(self.lista.GetStringSelection())
  self.Destroy()

 def populate_list(self):
  for i in self.urls:
   self.lista.Append(i)

class shorten(urlList):
 def __init__(self, urls, parent):
  urlList.__init__(self, urls)
  self.parent = parent

 def onGo(self, ev):
  self.parent.text.SetValue(self.parent.text.GetValue().replace(self.lista.GetStringSelection(), url_shortener.shorten(self.lista.GetStringSelection())))
  self.Destroy()

class unshorten(shorten):
 def __init__(self, urls, parent):
  shorten.__init__(self, urls, parent)

 def onGo(self, ev):
  self.parent.text.SetValue(self.parent.text.GetValue().replace(self.lista.GetStringSelection(), url_shortener.unshorten(self.lista.GetStringSelection())))
  self.Destroy()