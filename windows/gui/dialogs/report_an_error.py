# -*- coding: utf-8 -*-
import wx
import application
from suds.client import Client

class reportBug(wx.Dialog):
 def __init__(self):
  self.user = "informador"
  self.password = "contrasena"
  self.url = application.report_bugs_url
  wx.Dialog.__init__(self, None, -1)
  self.SetTitle(_(u"Report an error"))
  panel = wx.Panel(self)
  sizer = wx.BoxSizer(wx.VERTICAL)
  summaryLabel = wx.StaticText(panel, -1, u"Describe en pocas palabras lo que ha pasado (después podrás profundizar): ")
  self.summary = wx.TextCtrl(panel, -1)
  self.summary.SetFocus()
  descriptionLabel = wx.StaticText(panel, -1, u"Aquí puedes describir más detalladamente el error: ")
  self.description = wx.TextCtrl(panel, -1, style=wx.TE_MULTILINE)
  summaryBox = wx.BoxSizer(wx.HORIZONTAL)
  summaryBox.Add(summaryLabel)
  summaryBox.Add(self.summary)
  descBox = wx.BoxSizer(wx.HORIZONTAL)
  descBox.Add(descriptionLabel)
  descBox.Add(self.description)
  sizer.Add(summaryBox)
  sizer.Add(descBox)
  ok = wx.Button(panel, wx.ID_OK, u"Enviar reporte")
  ok.Bind(wx.EVT_BUTTON, self.onSend)
  cancel = wx.Button(panel, wx.ID_CANCEL, u"Cancelar")
  btnBox = wx.BoxSizer(wx.HORIZONTAL)
  btnBox.Add(ok)
  btnBox.Add(cancel)
  sizer.Add(btnBox)
  panel.SetSizer(sizer)

 def onSend(self, ev):
  if self.summary.GetValue() == "" or self.description.GetValue() == "":
   wx.MessageDialog(self, u"Debes llenar ambos campos.", u"Error", wx.OK|wx.ICON_ERROR).ShowModal()
   return
  try:
   client = Client(self.url)
   issue = client.factory.create('IssueData')
   issue.project.name = "TW Blue"
   issue.project.id = 0
   issue.summary = self.summary.GetValue(),
   issue.description = self.description.GetValue(),
   issue.category = "General"
   issue.reproducibility.name = "N/A"
   issue.severity.name = "minor"
   issue.priority.name = "normal"
   issue.view_state.name = "public"
   issue.resolution.name = "open"
   issue.projection.name = "none"
   issue.eta.name = "eta"
   issue.status.name = "new"
   id = client.service.mc_issue_add(self.user, self.password, issue)
   wx.MessageDialog(self, u"¡Gracias por reportarnos este error! En próximas versiones quizá puedas verlo entre la lista de cambios. Has reportado el error número %i." % (id), u"Reportado con éxito", wx.OK).ShowModal()
   self.EndModal(wx.ID_OK)
  except:
   wx.MessageDialog(self, -1, u"Algo malo pasó mientras intentábamos reportar tu error. Por favor, vuelve a intentarlo más tarde.", u"Error al reportar", wx.ICON_ERROR|wx.OK).ShowModal()
   self.EndModal(wx.ID_CANCEL)