# -*- coding: utf-8 -*-
import wx
import translator

class translateDialog(wx.Dialog):
 def __init__(self):
  wx.Dialog.__init__(self, None, -1, title=u"Translate message")
  panel = wx.Panel(self)
  sizer = wx.BoxSizer(wx.VERTICAL)
  staticSource = wx.StaticText(panel, -1, u"Source Language: ")
  self.source_lang = wx.ComboBox(panel, -1, choices=[x[1] for x in translator.available_languages()], style = wx.CB_READONLY)
  self.source_lang.SetFocus()
  staticDest = wx.StaticText(panel, -1, u"Target Language: ")
  self.source_lang.SetSelection(0)
  self.dest_lang = wx.ComboBox(panel, -1, choices=[x[1] for x in translator.available_languages()], style = wx.CB_READONLY)
  listSizer = wx.BoxSizer(wx.HORIZONTAL)
  listSizer.Add(staticSource)
  listSizer.Add(self.source_lang)
  listSizer.Add(staticDest)
  listSizer.Add(self.dest_lang)
  ok = wx.Button(panel, wx.ID_OK, u"OK")
#  ok.Bind(wx.EVT_BUTTON, self.onOk)
  cancel = wx.Button(panel, wx.ID_CANCEL, u"Cancel")
  self.SetEscapeId(wx.ID_CANCEL)

 def onOk(self, ev):
  self.EndModal(wx.ID_OK)
