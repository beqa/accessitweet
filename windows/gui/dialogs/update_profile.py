# -*- coding: utf-8 -*-
import wx
from twython import TwythonError

class updateProfile(wx.Dialog):
 def __init__(self, parent):
  self.twitter = parent.twitter
  self.parent = parent
  wx.Dialog.__init__(self, None, -1, size=(650,480))
  self.SetTitle(_(u"Update your profile"))
  panel = wx.Panel(self)
  labelName = wx.StaticText(panel, -1, _(u"Name (Maximum 20 characters)"))
  self.name = wx.TextCtrl(panel, -1, size=(250,250))
  self.name.SetFocus()
  labelLocation = wx.StaticText(panel, -1, _(u"Location: "))
  self.location = wx.TextCtrl(panel, -1)
  labelUrl = wx.StaticText(panel, -1, _(u"Website: "))
  self.url = wx.TextCtrl(panel, -1)
  labelDescription = wx.StaticText(panel, -1, _(u"Bio (Maximum 160 characters):"))
  self.description = wx.TextCtrl(panel, -1, size=(400, 400))
  ok = wx.Button(panel, wx.ID_OK, _(u"Update profile"))
  ok.Bind(wx.EVT_BUTTON, self.onUpdateProfile)
  close = wx.Button(panel, wx.ID_CANCEL, _("Close"))
#  close.Bind(wx.EVT_BUTTON, self.onClose)
  self.get_data()

 def onUpdateProfile(self, ev):
  try:
   f = self.twitter.twitter.update_profile(name=self.name.GetValue(), location=self.location.GetValue(), url=self.url.GetValue(), description=self.description.GetValue())
   self.EndModal(wx.ID_OK)
  except TwythonError as e:
   output.speak(u"Error %s. %s" % (e.error_code, e.msg))
   return

 def get_data(self):
  data = self.twitter.twitter.show_user(screen_name=self.parent.db.settings["user_name"])
  self.name.ChangeValue(data["name"])
  if data["url"] != None:
   self.url.ChangeValue(data["url"])
  if len(data["location"]) > 0:
   self.location.ChangeValue(data["location"])
  if len(data["description"]) > 0:
   self.description.ChangeValue(data["description"])
