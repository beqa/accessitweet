# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import wx
import twitter
import config
from twython import TwythonError
import sound
import urlList
import url_shortener
import translate, translator
import output
import transfer_dialogs, json
import audio

class textLimited(wx.Dialog):
 def __init__(self, message, title, text, parent):
  wx.Dialog.__init__(self, parent, size=(500,500))
  self.twitter = parent.twitter
  self.parent = parent
  self.SetTitle(_(u"Tweet - %i of 140") % (len(text)))
  self.panel = wx.Panel(self)

 def createControls(self, message, title, text):
  label = wx.StaticText(self.panel, -1, message)
  self.text = wx.TextCtrl(self.panel, -1, text, style=wx.TE_MULTILINE, size=(550,550))
  self.text.SetFocus()
  self.text.Bind(wx.EVT_TEXT, self.onTimer)
  textBox = wx.BoxSizer(wx.HORIZONTAL)
  textBox.Add(label)
  textBox.Add(self.text)
  mainBox = wx.BoxSizer(wx.VERTICAL)
  mainBox.Add(textBox)
  self.shortenButton = wx.Button(self.panel, -1, "Shorten URL")
  self.shortenButton.Bind(wx.EVT_BUTTON, self.onShorten)
  self.unshortenButton = wx.Button(self.panel, -1, "Unshorten URL")
  self.unshortenButton.Bind(wx.EVT_BUTTON, self.onUnshorten)
  self.shortenButton.Disable()
  self.unshortenButton.Disable()
  self.attach = wx.Button(self.panel, -1, u"Attach audio")
  self.attach.Bind(wx.EVT_BUTTON, self.onAttach)
  self.translateButton = wx.Button(self.panel, -1, u"Translate Tweet")
  self.translateButton.Bind(wx.EVT_BUTTON, self.onTranslate)
  self.okButton = wx.Button(self.panel, wx.ID_OK, "Tweet")
  self.okButton.Bind(wx.EVT_BUTTON, self.onSend)
  cancelButton = wx.Button(self.panel, wx.ID_CANCEL, "Cancel")
  cancelButton.Bind(wx.EVT_BUTTON, self.onCancel)
  buttonsBox = wx.BoxSizer(wx.HORIZONTAL)
  buttonsBox.Add(self.shortenButton)
  buttonsBox.Add(self.unshortenButton)
  buttonsBox.Add(self.okButton)
  buttonsBox.Add(cancelButton)
  mainBox.Add(buttonsBox)
  selectId = wx.NewId()
  self.Bind(wx.EVT_MENU, self.onSelect, id=selectId)
  self.accel_tbl = wx.AcceleratorTable([
(wx.ACCEL_CTRL, ord('A'), selectId),
])
  self.SetAcceleratorTable(self.accel_tbl)
  self.panel.SetSizer(mainBox)
  self.Bind(wx.EVT_CHAR_HOOK, self.onEscape, self.text)

 def onAttach(self, ev):
  ev.Skip()
  self.recording = audio.audioDialog(self.parent)
  if self.recording.ShowModal() != wx.ID_OK:
   self.recording.cleanup()
   return
  self.recording.postprocess()
  output.speak(u"Attaching Audio...")
  base_url = 'http://sndup.net/post.php'
  if len(config.main["sndup"]["api_key"]) > 0:
   url = base_url + '?apikey=' + config.main['sndup']['api_key']
  else:
   url = base_url
  self.upload_dlg = transfer_dialogs.UploadDialog(parent=self.parent, title="Uploading Audio...", field='file', url=url, filename=self.recording.file, completed_callback=self.upload_completed)
  self.upload_dlg.Show()
  self.upload_dlg.perform_threaded()

 def upload_completed(self):
  url = json.loads(self.upload_dlg.response['body'])['url']
  self.upload_dlg.Destroy()
  if url != 0:
   self.text.SetValue(self.text.GetValue()+url+" #audio")
  else:
   output.speak(u"could not upload the audio.")

 def onTranslate(self, ev):
  dlg = translate.translateDialog()
  selection = dlg.ShowModal()
  if selection != wx.ID_CANCEL:
   text_to_translate = self.text.GetValue().encode("utf-8")
   source = [x[0] for x in translator.available_languages()][dlg.source_lang.GetSelection()]
   dest = [x[0] for x in translator.available_languages()][dlg.dest_lang.GetSelection()]
   t = translator.translator.Translator()
   t.from_lang = source
   t.to_lang = dest
   msg = t.translate(text_to_translate)
   self.text.ChangeValue(msg)
   output.speak(u"Translated.")
   self.text.SetFocus()
  else:
   return
  dlg.Destroy()

 def onSelect(self, ev):
  self.text.SelectAll()

 def onShorten(self, ev):
  urls =  twitter.utils.find_urls_in_text(self.text.GetValue())
  if len(urls) == 0:
   output.speak(u"No URL to shorten", True)
  elif len(urls) == 1:
   self.text.SetValue(self.text.GetValue().replace(urls[0], url_shortener.shorten(urls[0])))
   output.speak(u"URL Shortened")
  elif len(urls) > 1:
   urlList.shorten(urls, self).ShowModal()
  self.text.SetFocus()

 def onUnshorten(self, ev):
  urls =  twitter.utils.find_urls_in_text(self.text.GetValue())
  if len(urls) == 0:
   output.speak(u"No URL for unshortening", True)
  elif len(urls) == 1:
   self.text.SetValue(self.text.GetValue().replace(urls[0], url_shortener.unshorten(urls[0])))
   output.speak(u"URL unshortened")
  elif len(urls) > 1:
   urlList.unshorten(urls, self).ShowModal()
  self.text.SetFocus()

 def onEscape(self, ev):
  if ev.GetKeyCode() == wx.WXK_RETURN:
   self.onSend(wx.EVT_BUTTON)
  ev.Skip()

 def onTimer(self, ev):
  self.SetTitle("Tweet - %i of 140" % (len(self.text.GetValue())))
  if len(self.text.GetValue()) > 1:
   self.shortenButton.Enable()
   self.unshortenButton.Enable()
  else:
   self.shortenButton.Disable()
   self.unshortenButton.Disable()
  if len(self.text.GetValue()) > 140:
   self.parent.parent.sound.play("max_length.wav")
   self.okButton.Disable()
  elif len(self.text.GetValue()) <= 140:
   self.okButton.Enable()

 def onCancel(self, ev):
  self.Destroy()

class tweet(textLimited):
 def createControls(self, message, title, text):
  label = wx.StaticText(self.panel, -1, message)
  self.text = wx.TextCtrl(self.panel, -1, text, style=wx.TE_MULTILINE, size=(550,550))
  self.text.SetFocus()
  self.text.Bind(wx.EVT_TEXT, self.onTimer)
  textBox = wx.BoxSizer(wx.HORIZONTAL)
  textBox.Add(label)
  textBox.Add(self.text)
  mainBox = wx.BoxSizer(wx.VERTICAL)
  mainBox.Add(textBox)
  self.shortenButton = wx.Button(self.panel, -1, "Shorten URL")
  self.shortenButton.Bind(wx.EVT_BUTTON, self.onShorten)
  self.unshortenButton = wx.Button(self.panel, -1, "Unshorten URL")
  self.unshortenButton.Bind(wx.EVT_BUTTON, self.onUnshorten)
  self.shortenButton.Disable()
  self.unshortenButton.Disable()
  self.upload_image = wx.Button(self.panel, -1, _(u"Upload immage"))
  self.upload_image.Bind(wx.EVT_BUTTON, self.onUpload_image)
  self.attach = wx.Button(self.panel, -1, u"Attach audio")
  self.attach.Bind(wx.EVT_BUTTON, self.onAttach)
  self.translateButton = wx.Button(self.panel, -1, u"Translate Tweet")
  self.translateButton.Bind(wx.EVT_BUTTON, self.onTranslate)
  self.okButton = wx.Button(self.panel, wx.ID_OK, "Tweet")
  self.okButton.Bind(wx.EVT_BUTTON, self.onSend)
  cancelButton = wx.Button(self.panel, wx.ID_CANCEL, "Cancel")
  cancelButton.Bind(wx.EVT_BUTTON, self.onCancel)
  buttonsBox = wx.BoxSizer(wx.HORIZONTAL)
  buttonsBox.Add(self.shortenButton)
  buttonsBox.Add(self.unshortenButton)
  buttonsBox.Add(self.upload_image)
  buttonsBox.Add(self.okButton)
  buttonsBox.Add(cancelButton)
  mainBox.Add(buttonsBox)
  selectId = wx.NewId()
  self.Bind(wx.EVT_MENU, self.onSelect, id=selectId)
  self.accel_tbl = wx.AcceleratorTable([
(wx.ACCEL_CTRL, ord('A'), selectId),
])
  self.SetAcceleratorTable(self.accel_tbl)
  self.panel.SetSizer(mainBox)
  self.Bind(wx.EVT_CHAR_HOOK, self.onEscape, self.text)

 def __init__(self, message, title, text, parent):
  textLimited.__init__(self, message, title, text, parent)
  self.image = None
  self.createControls(message, title, text)
  self.onTimer(wx.EVT_CHAR_HOOK)

 def onUpload_image(self, ev):
  if self.upload_image.GetLabel() == "Discard image":
   self.image = None
   del self.file
   output.speak(_(u"Discarded."))
   self.upload_image.SetLabel(_(u"Upload a picture"))
  else:
   openFileDialog = wx.FileDialog(self, _(u"Select a picture to upload it"), "", "", _("Image Files (*.png, *.jpg, *.gif)|*.png; *.jpg; *.gif"), wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
   if openFileDialog.ShowModal() == wx.ID_CANCEL:
    return
   self.file = open(openFileDialog.GetPath(), "rb")
   self.image = True
   self.upload_image.SetLabel(_(u"Discard image"))
  ev.Skip()

 def onSend(self, ev):
  try:
   if self.image == None:
    self.twitter.twitter.update_status(status=self.text.GetValue())
   else:
    self.twitter.twitter.update_status_with_media(status=self.text.GetValue(), media=self.file)
   self.parent.parent.sound.play("tweet_send.wav")
   self.Destroy()
  except TwythonError as err:
   output.speak("Error %s: %s" % (err.error_code, err.msg), True)

class retweet(tweet):
 def __init__(self, message, title, text, parent):
  tweet.__init__(self, message, title, text, parent)
  self.createControls(message, title, text)
  self.in_reply_to = parent.db.settings[parent.name_buffer][parent.get_selected()]["id"]

 def onSend(self, ev):
  try:
   if self.image == None:
    self.twitter.twitter.update_status(status=self.text.GetValue(), in_reply_to_status_id=self.in_reply_to)
   else:
    self.twitter.twitter.update_status_with_media(status=self.text.GetValue(), in_reply_to_status_id=self.in_reply_to, media=self.file)
   self.parent.parent.sound.play("retweet_send.wav")
   self.Destroy()
  except TwythonError as err:
   output.speak("Error %s: %s" % (err.error_code, err.msg), True)

class dm(textLimited):
 def __init__(self, message, title, text, parent):
  self.parent = parent
  textLimited.__init__(self, message, title, text, parent)
  if self.parent.name_buffer == "followers" or self.parent.name_buffer == "friends":
   list = [self.parent.db.settings[self.parent.name_buffer][self.parent.get_selected()]["screen_name"]]
  else:
   list =twitter.utils.get_all_users(self.parent.db.settings[self.parent.name_buffer][self.parent.get_selected()], self.parent.db)
  label = wx.StaticText(self.panel, -1, u"Recipient")
  self.cb = wx.ComboBox(self.panel, -1, choices=list, value=list[0])
  self.createControls(message, title, text)

 def onSend(self, ev):
  try:
   self.twitter.twitter.send_direct_message(text=self.text.GetValue(), screen_name=self.cb.GetValue())
   self.parent.parent.sound.play("dm_sent.wav")
   self.Destroy()
  except TwythonError as err:
   output.speak("Error %s: %s" % (err.error_code, err.msg), True)

class reply(textLimited):
 def __init__(self, message, title, text, parent):
  textLimited.__init__(self, message, title, text, parent)
  self.createControls(message, title, text)
  self.text.SetInsertionPoint(len(self.text.GetValue()))
  self.mentionAll = wx.Button(self, -1, u"Mention All")
  self.mentionAll.Disable()
  self.mentionAll.Bind(wx.EVT_BUTTON, self.mentionAllUsers)
  self.check_if_users()

 def check_if_users(self):
  try:
   if len(self.parent.db.settings[self.parent.name_buffer][self.parent.get_selected()]["entities"]["user_mentions"]) > 0:
    self.mentionAll.Enable()
  except KeyError:
   pass

 def mentionAllUsers(self, ev):
  self.text.SetValue(self.text.GetValue()+twitter.utils.get_all_mentioned(self.parent.db.settings[self.parent.name_buffer][self.parent.get_selected()], self.parent.db))
  self.text.SetFocus()

 def onSend(self, ev):
  try:
   self.twitter.twitter.update_status(status=self.text.GetValue(), in_reply_to_status_id=self.parent.db.settings[self.parent.name_buffer][self.parent.get_selected()]["id"])
   self.parent.parent.sound.play("tweet_send.wav")
   self.Destroy()
  except TwythonError as err:
   output.speak("Error %s: %s" % (err.error_code, err.msg), True)
