# -*- coding: utf-8 -*-
import wx
from mysc import event
import gui.dialogs
import twitter
import webbrowser
from twython import TwythonError
from mysc.thread_utils import call_threaded
import config
import sound
import url_shortener
import logging as original_logger
log = original_logger.getLogger("buffers.base")
import output
import platform

class basePanel(wx.Panel):
 
# Multiplatform widgets. For GNU/Linux we need a ListBox control while Windows uses ListCtrl.

 def create_list(self):
  """ Returns the list for put the tweets here."""
  if self.system == "Windows":
   wxList = wx.ListCtrl(self, -1, size=(800,800), style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
   wxList.InsertColumn(0, _(u"User"))
   wxList.InsertColumn(1, _(u"Text"))
   wxList.InsertColumn(2, _(u"Date"))
   wxList.InsertColumn(3, _(u"Client"))
   return wxList
  elif self.system == "Linux":
   return wx.ListBox(self, -1, choices=[])

 def insert_item(self, item, reversed):
  """ Inserts an item on the list, depending on the OS."""
  if self.system == "Windows":
   if reversed == False: items = self.list.GetItemCount()
   else: items = 0
   self.list.InsertStringItem(items, item[0])
   self.list.SetStringItem(items, 1, item[1])
   self.list.SetStringItem(items, 2, item[2])
   self.list.SetStringItem(items, 3, item[3])
  elif self.system == "Linux":
   self.list.Append(" ".join(item))

 def remove_item(self, pos):
  """ Deletes an item from the list."""
  if self.system == "Windows":
   if pos > 0: self.list.Focus(pos-1)
   self.list.DeleteItem(pos)
  elif self.system == "Linux":
   if pos > 0: self.list.SetSelection(pos-1)
   self.list.Delete(pos)

 def clear_list(self):
  if self.system == "Windows":
   self.list.DeleteAllItems()
  elif self.system == "Linux":
   self.list.Clear()

 def get_selected(self):
  if self.system == "Windows":
   return self.list.GetFocusedItem()
  elif self.system == "Linux":
   return self.list.GetSelection()

 def select_item(self, pos):
  if self.system == "Windows":
   self.list.Focus(pos)
  elif self.system == "Linux":
   self.list.SetSelection(pos)

 def bind_events(self):
  self.Bind(event.EVT_RESULT, self.update)
  self.Bind(event.MyEVT_DELETED, self.Remove)
  self.list.Bind(wx.EVT_CHAR_HOOK, self.interact)
  if self.system == "Windows":
   self.list.Bind(wx.EVT_LIST_ITEM_FOCUSED, self.onFocus)
  elif self.system == "Linux":
   self.list.Bind(wx.EVT_LISTBOX, self.onFocus)

 def get_count(self):
  if self.system == "Windows":
   return self.list.GetItemCount()
  elif self.system == "Linux":
   return self.list.GetCount()

 def get_message(self):
  return " ".join(self.compose_function(self.db.settings[self.name_buffer][self.get_selected()], self.db))

 def __init__(self, parent, window, name_buffer, function, argumento=None, sound="", timeline=False):
  self.db = window.db
  self.twitter = window.twitter
  self.name_buffer = name_buffer
  self.function = function
  self.argumento = argumento
  self.sound = sound
  self.parent = window
  self.compose_function = twitter.compose.compose_tweet
  self.system = platform.system()
  wx.Panel.__init__(self, parent)
  sizer = wx.BoxSizer()
  self.list = self.create_list()
#  sizer.Add(sizer)
  self.bind_events()

 def Remove(self, ev):
#  try:
  self.remove_item(ev.GetItem())
#  except:
#   log.error(u"Cannot delete the %s item from list " % str(ev.GetItem()))

 def destroy_status(self, ev):
  index = self.get_selected()
  try:
   self.twitter.twitter.destroy_status(id=self.db.settings[self.name_buffer][index]["id"])
   self.db.settings[self.name_buffer].pop(index)
   self.remove_item(index)
   if index > 0:
    self.select_item(index-1)
  except:
   snd = self.parent.sound.play("error.wav")

 def onFocus(self, ev):
  if self.db.settings[self.name_buffer][self.get_selected()].has_key("retweeted_status"): tweet = self.db.settings[self.name_buffer][self.get_selected()]["retweeted_status"]
  else: tweet = self.db.settings[self.name_buffer][self.get_selected()]
  if twitter.utils.is_audio(tweet):
   if config.main["streams"]["play_audio_sound"] == True:
    snd = self.parent.sound.play("audio.wav", False)

 def start_streams(self):
  if self.name_buffer == "sent":
   num = twitter.starting.start_sent(self.db, self.twitter, self.name_buffer, self.function, param=self.argumento, sndFile=self.sound)
  else:
   num = twitter.starting.start_stream(self.db, self.twitter, self.name_buffer, self.function, param=self.argumento, sndFile=self.sound)
  return num

 def put_items(self, num):
  if self.get_count() == 0:
   for i in self.db.settings[self.name_buffer]:
    tweet = self.compose_function(i, self.db)
    self.insert_item(tweet, reversed=False)
   self.set_list_position()
  elif self.get_count() > 0:
   if config.main["streams"]["reverse_timelines"] == False:
    for i in self.db.settings[self.name_buffer][:num]:
     tweet = self.compose_function(i, self.db)
     self.insert_item(tweet, reversed=False)
   else:
    for i in self.db.settings[self.name_buffer][0:num]:
     tweet = self.compose_function(i, self.db)
     self.insert_item(tweet, reversed=True)

 def onDm(self, ev):
  if self.name_buffer == "sent": return
  if self.name_buffer == "direct_messages":
   self.onResponse(ev)
  else:
   gui.dialogs.message.dm(_("Direct message to %s") % (self.db.settings[self.name_buffer][self.get_selected()]["user"]["screen_name"]), "", "", self).ShowModal()
  if ev != None:
   self.list.SetFocus()

 def post_status(self, ev=None):
  if ev != None:
   text = gui.dialogs.message.tweet(_(u"Write the tweet here"), _(u"Tweet"), "", self).ShowModal()
   self.list.SetFocus()
  else:
   text = gui.dialogs.message.tweet(_(u"Write the tweet here"), _(u"Tweet"), "", self).Show()

 def onRetweet(self, ev):
  if self.name_buffer != "direct_messages":
   dlg = wx.MessageDialog(self.parent, _(u"Would you like to add a comment to this tweet?"), _("Retweet"), wx.YES_NO|wx.CANCEL|wx.ICON_QUESTION)
   response = dlg.ShowModal()
   if response == wx.ID_YES:
    gui.dialogs.message.retweet(_(u"Add your comment to the tweet"), "", u"“@%s: %s ”" % (self.db.settings[self.name_buffer][self.get_selected()]["user"]["screen_name"], self.db.settings[self.name_buffer][self.get_selected()]["text"]), self).ShowModal()
    dlg.Destroy()
    if ev != None:
     self.list.SetFocus()
   elif response == wx.ID_NO:
    try:
     self.twitter.twitter.retweet(id=self.db.settings[self.name_buffer][self.get_selected()]["id"])
     self.parent.sound.play("retweet_send.wav")
     if ev != None:
      self.list.SetFocus()
    except TwythonError as e:
     output.speak("Error %s: %s" % (e.error_code, e.msg), True)
     if ev != None: self.list.SetFocus()
   else:
    return
   dlg.Destroy()

 def onResponse(self, ev):
  if self.name_buffer == "sent": return
  gui.dialogs.message.reply(_(u"Reply to %s ") % (self.db.settings[self.name_buffer][self.get_selected()]["user"]["screen_name"]), "", u"@%s " % (self.db.settings[self.name_buffer][self.get_selected()]["user"]["screen_name"]), self).ShowModal()
  if ev != None:   self.list.SetFocus()

 def update(self, ev):
  if config.main["streams"]["reverse_timelines"] == False: tweet = self.compose_function(self.db.settings[self.name_buffer][-1], self.db)
  else:
   tweet = self.compose_function(self.db.settings[self.name_buffer][0], self.db)
  self.insert_item(tweet, reversed=config.main["streams"]["reverse_timelines"])

 def interact(self, ev):
  try:
   if self.db.settings[self.name_buffer][self.get_selected()].has_key("retweeted_status"):  tweet = self.db.settings[self.name_buffer][self.get_selected()]["retweeted_status"]
   else: tweet = self.db.settings[self.name_buffer][self.get_selected()]
   urls = twitter.utils.find_urls(tweet)
  except:
   urls = []
  if type(ev) is str: event = ev
  else:
   if ev.GetKeyCode() == wx.WXK_RETURN and ev.ControlDown(): event = "audio"
   elif ev.GetKeyCode() == wx.WXK_RETURN: event = "url"
   elif ev.GetKeyCode() == wx.WXK_F5: event = "volume_down"
   elif ev.GetKeyCode() == wx.WXK_F6: event = "volume_up"
   elif ev.GetKeyCode() == wx.WXK_DELETE and ev.ShiftDown(): event = "clear_list"
   elif ev.GetKeyCode() == wx.WXK_DELETE: event = "delete_item"
   else:
    ev.Skip()
    return
  if event == "audio"  and len(urls) > 0:
   self.streamer(urls[0])
  elif event == "url":
   if len(urls) == 0: return
   elif len(urls) == 1:
    output.speak(_(u"Opening URL..."), True)
    webbrowser.open_new_tab(urls[0])
   elif len(urls) > 1:
    gui.dialogs.urlList.urlList(urls).ShowModal()
  elif event == "volume_down":
   if config.main["mysc"]["volume"] > 0.05:
    config.main["mysc"]["volume"] = config.main["mysc"]["volume"]-0.05
    snd = self.parent.sound.play("volume_changed.wav", False)
    if hasattr(self.parent, "audioStream"):
     self.parent.audioStream.stream.volume = config.main["mysc"]["volume"]
  elif event == "volume_up":
   if config.main["mysc"]["volume"] < 0.95:
    config.main["mysc"]["volume"] = config.main["mysc"]["volume"]+0.05
    snd = self.parent.sound.play("volume_changed.wav", False)
    if hasattr(self.parent, "audioStream"):
     self.parent.audioStream.stream.volume = config.main["mysc"]["volume"]
  elif event == "clear_list":
   dlg = wx.MessageDialog(self, _(u"Do you really want clear this buffer? All Tweets will be deleted from this buffer, but not from Twitter."), _(u"Clear"), wx.ICON_QUESTION|wx.YES_NO)
   if dlg.ShowModal() == wx.ID_YES:
    self.db.settings[self.name_buffer] = []
    self.clear_list()
  elif event == "delete_item" and self.name_buffer == "favs":
   dlg = wx.MessageDialog(self, _(u"Do you really want delete this message? It will be deleted from twitter too."), _(u"Delete"), wx.ICON_QUESTION|wx.YES_NO)
   if dlg.ShowModal() == wx.ID_YES:
    self.destroy_status(wx.EVT_MENU)
   else:
    return
  try:
   ev.Skip()
  except:
   pass

 def streamer(self, url):
  if hasattr(self.parent, "audioStream"):
   if self.parent.audioStream.stream.is_active() == 0:
    output.speak(_(u"Playing..."))
    self.parent.audioStream = sound.urlStream(url)
    try:
     self.parent.audioStream.prepare()
     self.parent.audioStream.play()
    except:
     del self.parent.audioStream
     output.speak(_(u"Can't play the sound."))
   else:
    output.speak(_(u"Stopped."))
    self.parent.audioStream.stream.stop()
  else:
   output.speak(_(u"Playing..."))
   self.parent.audioStream = sound.urlStream(url)
   try:
    self.parent.audioStream.prepare()
    self.parent.audioStream.play()
   except:
    output.speak(_(u"Can't play the sound."))
    del self.parent.audioStream

 def set_list_position(self):
  if config.main["streams"]["reverse_timelines"] == False:
   self.select_item(len(self.db.settings[self.name_buffer])-1)
  else:
   self.select_item(0)

class dmPanel(basePanel):
 def __init__(self, parent, window, name_buffer, function, argumento=None, sound=""):
  basePanel.__init__(self, parent, window, name_buffer, function, argumento=argumento, sound=sound)

 def destroy_status(self, ev):
  index = self.get_selected()
  try:
   self.twitter.twitter.destroy_direct_message(id=self.db.settings[self.name_buffer][index]["id"])
   self.db.settings[self.name_buffer].pop(index)
   self.remove_item(index)
  except:
   self.parent.sound.play("error.wav")

 def onResponse(self, ev):
  gui.dialogs.message.dm(_(u"Direct Message to  %s ") % (self.db.settings[self.name_buffer][self.get_selected()]["sender"]["screen_name"]), "", "", self).ShowModal()
  if ev != None: self.list.SetFocus()

class followersPanel(basePanel):
 def create_list(self):
  """ Returns the list for put the tweets here."""
  if self.system == "Windows":
   wxList = wx.ListCtrl(self, -1, size=(800,800), style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
   wxList.InsertColumn(0, _(u"User"))
   return wxList
  elif self.system == "Linux":
   return wx.ListBox(self, -1, choices=[])

 def insert_item(self, item, reversed):
  """ Inserts an item on the list, depending on the OS."""
  if self.system == "Windows":
   if reversed == False: items = self.list.GetItemCount()
   else: items = 0
   self.list.InsertStringItem(items, item[1])
  elif self.system == "Linux":
   self.list.Append(" ".join(item))

 def bind_events(self):
  self.Bind(event.EVT_RESULT, self.update)
  self.list.Bind(wx.EVT_CHAR_HOOK, self.interact)

 def __init__(self, parent, window, name_buffer, function, argumento=None, sound="", timeline=False):
  basePanel.__init__(self, parent, window, name_buffer, function, argumento=argumento, sound=sound)
  self.compose_function = twitter.compose.compose_followers_list

 def onResponse(self, ev):
  gui.dialogs.message.reply(_(u"Mention to %s ") % (self.db.settings[self.name_buffer][self.get_selected()]["screen_name"]), "", u"@%s " % (self.db.settings[self.name_buffer][self.get_selected()]["screen_name"]), self).ShowModal()
  if ev != None:   self.list.SetFocus()

 def Remove(self, ev):
  try:
   index = self.get_selected()
   self.remove_item(ev.GetItem())
  except:
   log.error("Imposible eliminar el item %s de la lista" % str(ev.GetItem()))

 def start_streams(self):
  num = twitter.starting.start_followers(self.db, self.twitter, self.name_buffer, self.function, param=self.argumento, sndFile=self.sound)
  return num

 def put_items(self, num):
  if self.get_count() > 0:
   if self.system == "Windows": self.list.DeleteAllItems()
  elif self.system == "Linux": self.list.Clear()
  for i in self.db.settings[self.name_buffer][:num]:
   f = self.compose_function(i, self.db)
   self.insert_item(f, reversed=config.main["streams"]["reverse_timelines"])
  self.set_list_position()

 def interact(self, ev):
  if type(ev) is str: event = ev
  else:
   if ev.GetKeyCode() == wx.WXK_RETURN:
    event = "url"
   elif ev.GetKeyCode() == wx.WXK_F5:
    event = "volume_down"
   elif ev.GetKeyCode() == wx.WXK_F6:
    event = "volume_down"
   else:
    ev.Skip()
    return
  if event == "url":
   gui.dialogs.show_user.showUserProfile(self.parent.twitter, self.db.settings[self.name_buffer][self.get_selected()]["screen_name"]).ShowModal()
  elif event == "volume_down":
   if config.main["mysc"]["volume"] > 0.05:
    config.main["mysc"]["volume"] = config.main["mysc"]["volume"]-0.05
    self.parent.sound.play("volume_changed.wav")
  elif event == "volume_up":
   if config.main["mysc"]["volume"] < 0.95:
    config.main["mysc"]["volume"] = config.main["mysc"]["volume"]+0.05
    self.parent.sound.play("volume_changed.wav")
  if type(ev) is not str: ev.Skip()

class eventsPanel(wx.Panel):

 def put_items(self, items):
  pass

 def create_list(self):
  """ Returns the list for put the tweets here."""
  if self.system == "Windows":
   wxList = wx.ListCtrl(self, -1, size=(800,800), style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
   wxList.InsertColumn(0, _(u"Date"))
   wxList.InsertColumn(1, _(u"EVent"))
   return wxList
  elif self.system == "Linux":
   return wx.ListBox(self, -1, choices=[])

 def insert_item(self, item, reversed):
  """ Inserts an item on the list, depending on the OS."""
  if self.system == "Windows":
   if reversed == False: items = self.list.GetItemCount()
   else: items = 0
   self.list.InsertStringItem(items, item[0])
   self.list.SetStringItem(items, 1, item[1])
  elif self.system == "Linux":
   self.list.Append(" ".join(item))

 def remove_item(self, pos):
  """ Deletes an item from the list."""
  if self.system == "Windows":
   if pos > 0: self.list.Focus(pos-1)
   self.list.DeleteItem(pos)
  elif self.system == "Linux":
   if pos > 0: self.list.SetSelection(pos-1)
   self.list.Delete(pos)

 def clear_list(self):
  if self.system == "Windows":
   self.list.DeleteAllItems()
  elif self.system == "Linux":
   self.list.Clear()

 def get_selected(self):
  if self.system == "Windows":
   return self.list.GetFocusedItem()
  elif self.system == "Linux":
   return self.list.GetSelection()

 def get_selected_text(self):
  if self.get_count() == 0: return _(u"Empty")
  if self.system == "Windows":
   return "%s. %s" % (self.list.GetItemText(self.get_selected()), self.list.GetItemText(self.get_selected(), 1))
  elif self.system == "Linux":
   return self.list.GetStringSelection()

 def select_item(self, pos):
  if self.system == "Windows":
   self.list.Focus(pos)
  elif self.system == "Linux":
   self.list.SetSelection(pos)

 def get_count(self):
  if self.system == "Windows":
   return self.list.GetItemCount()
  elif self.system == "Linux":
   return self.list.GetCount()

 def get_message(self):
  return self.get_selected_text()

 def __init__(self, parent):
  self.system = platform.system()
  self.name_buffer = "events"
  wx.Panel.__init__(self, parent)
  sizer = wx.BoxSizer()
  self.list = self.create_list()
  self.Bind(event.MyEVT_DELETED, self.update)

 def on_delete_event(self, ev):
  self.remove_item(self.get_selected())

 def start_streams(self):
  return 0

 def post_status(self, ev=None):
  if ev != None:
   text = gui.dialogs.message.tweet(_(u"Write the Tweet here"), "Tweet", "", self).ShowModal()
   self.list.SetFocus()
  else:
   text = gui.dialogs.message.tweet(_(u"Write the Tweet here"), "Tweet", "", self).Show()

 def update(self, ev):
  tweet = ev.GetItem()
  self.insert_item(tweet, reversed=config.main["streams"]["reverse_timelines"])
