# -*- coding: utf-8 -*-
""" Setup file to create executables and distribute the source code of this application. Don't forget this file! """
from setuptools import setup, find_packages
import py2exe
import os
#import application


def get_data():
 return ["conf.defaults", "cacert.pem", "nvdaControllerClient32.dll"]

if __name__ == '__main__':
 setup(
  name = "Accessitweet",
  author = "MTG Studios",
  author_email = "masonarm@gmail.com",
  version = "0.2",
  url = 'http://mtgstudiosus.tk',
packages=find_packages() ,
options = {
#"dist_dir": "compiled",
   'py2exe': {   
 #"includes": ['wx', 'durus', 'twython', 'config', 'configuration', 'gui', 'db',  'thread_utils', 'sound', 'paths', 'event', 'twitter'],
#'compressed': True,
#    'optimize': 1,
    'skip_archive': True,
   },
  },
  windows = [
   {
    'script': 'main.py',
    'dest_base': 'Accessitweet',
}
  ],
  install_requires = [
  ]
 )

